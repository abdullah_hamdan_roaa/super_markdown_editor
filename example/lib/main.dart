import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:super_markdown_editor/super_markdown_editor.dart';
import 'package:flutter_markdown/flutter_markdown.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Flutter Super Editor Demo',
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        Locale('en'), // English
        Locale('ar'), // Spanish
      ],
      locale: Locale("en"),
      home: MyHomePage(title: 'Flutter Super Editor Demo'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // final TextEditingController _textEditingController = TextEditingController(
  //     text: "# H1wdfwerwe\n"
  //         "## H2werwerwer\n"
  //         "### H3werwerwe\n"
  //         "### H3werwer\n"
  //         "### H3werwer\n"
  //         "### H3werwer\n"
  //         "| Name     | Age | City      |\n"
  //     "|----------|-----|-----------|\n"
  // "| John     | 25  | New York  |\n"
  // "| Emily    | 32  | London    |\n"
  // "| Michael  | 40  | Sydney    |\n"
  //         "# H1wdfwerwe\n"
  //         "## H2werwerwer\n"
  //         "### H3werwerwe\n"
  //         "### H3werwer\n"
  //         "### H3werwer\n"
  //
  // );

  // final TextEditingController _textEditingController = TextEditingController(text:
  // "| Name     | Age | City | City      |\n|----------|-----|-----------|-----------|\n| John     | 25  | New York  | New York  |"
  //     "\n| Emily    | 32  | London    | London    |\n| Michael  | 40  | Sydney  || Sydney  |\n"
  // );

  // final TextEditingController _textEditingController = TextEditingController(text: "![]"
  //     "(https://images.unsplash.com/photo-1575936123452-b67c3203c357?ixlib=rb-4.0"
  // ".3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8aW1hZ2V8ZW58MHx8MHx8fDA%3D&w=1000&q"
  // "=80)");


  final TextEditingController _textEditingController = TextEditingController(text: "~~text~~**bb*"
      "*");
  final FocusNode _node = FocusNode(canRequestFocus: true);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme
            .of(context)
            .colorScheme
            .inversePrimary,
        title: Text(widget.title),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Form(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SuperMarkdownEditor(
                  hintStyle: TextStyle(color: Colors.red),
                  viewMode: false,
                  minHeight: 100,
                  editorDecoration: BoxDecoration(
                    border: Border.all(
                        width: 1,
                        color: Colors.grey
                    ),
                  ),
                  focusNode: _node,
                  textController: _textEditingController,
                  textStyle:  TextStyle(
                  color: Colors.green,
                  //fontWeight: FontWeight.w400,
                  fontSize: 12,
                  overflow: TextOverflow.ellipsis),
                  showTableBuilder: (context, widget, onSave) {
                    showDialog(context: context, builder: (context) {
                      return Column(
                        children: [
                          widget,
                          const SizedBox(height: 20,),
                          FloatingActionButton(
                              child: Text("save"),
                              onPressed: () {
                            onSave();
                          })
                        ],
                      );
                    });
                  },
                  onLaunchUrl: (string) {},
                  onChanged: (text) {
                  },
                  onDownloadClick: (url) {
                  },
                  onPickImage: () {
                    return Future(() =>
                    "https://images.unsplash.com/photo-1575936123452-b67c3203c357?ixlib=rb-4.0"
                        ".3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8aW1hZ2V8ZW58MHx8MHx8fDA%3D&w=1000&q"
                        "=80");
                  },
                  onPickFile: () {
                    return Future(() => null);
                    },
                  linkActionBuilder: (bool , onAdd ) {
                    return InkWell(
                        onTap: onAdd,
                        child: Container(
                      width: 40,height: 40,color: Colors.red,
                    ));
        },
                ),
                const SizedBox(
                  height: 20,
                ),
                // FloatingActionButton(onPressed: () {
                //   setState(() {});
                // }),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(),
                SizedBox(height: 400, width: 300, child: Markdown(data: _textEditingController.text))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
