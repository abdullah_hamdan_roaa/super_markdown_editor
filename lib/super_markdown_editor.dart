library super_markdown_editor;
import 'package:markdown/markdown.dart' as md;
export 'src/super_markdown_editor_widget.dart';

String markdownToPlainText(String markdown) {
  // Replace bold text with plain text
  markdown = markdown.replaceAllMapped(RegExp(r'\*\*(.+?)\*\*'), (match) => match.group(1) ?? "");
  markdown = markdown.replaceAllMapped(RegExp('__(.+?)__'), (match) => match.group(1) ?? "");

  // Replace italicized text with plain text
  markdown = markdown.replaceAllMapped(RegExp('_(.+?)_'), (match) => match.group(1) ?? "");
  markdown = markdown.replaceAllMapped(RegExp(r'\*(.+?)\*'), (match) => match.group(1) ?? "");

  // Replace strikethrough text with plain text
  markdown = markdown.replaceAllMapped(RegExp('~~(.+?)~~'), (match) => match.group(1) ?? "");

  // Replace inline code blocks with plain text
  markdown = markdown.replaceAllMapped(RegExp('`(.+?)`'), (match) => match.group(1) ?? "");

  // Replace code blocks with plain text
  markdown =
      markdown.replaceAll(RegExp(r'```[\s\S]*?```', multiLine: true), '');
  markdown =
      markdown.replaceAll(RegExp(r'```[\s\S]*?```', multiLine: true), '');

  // Remove links
  markdown = markdown.replaceAll(RegExp(r'\[(.+?)\]\((.+?)\)'), '');

  // Remove images
  markdown = markdown.replaceAll(RegExp(r'!\[(.+?)\]\((.+?)\)'), '');

  // Remove headings
  markdown = markdown.replaceAll(RegExp(r'^#(.+?)\s*$', multiLine: true), '');
  markdown = markdown.replaceAll(RegExp(r'^#(.+?)\s*$', multiLine: true), '');
  markdown = markdown.replaceAll(RegExp(r'^#(.+?)\s*$', multiLine: true), '');


  // Remove blockquotes
  markdown =
      markdown.replaceAll(RegExp('#(.+?)', multiLine: true), '');

  // Remove lists
  markdown = markdown.replaceAll(
    RegExp(r'^\s*[\*\+-]\s+(.+?)\s*$', multiLine: true),
    '',
  );
  markdown = markdown.replaceAll(
    RegExp(r'^\s*\d+\.\s+(.+?)\s*$', multiLine: true),
    '',
  );

  // Remove table patterns
  markdown = markdown.replaceAll(RegExp(r'^\|.*\|$', multiLine: true), '');


  // Remove horizontal lines
  markdown =
      markdown.replaceAll(RegExp(r'^\s*[-*_]{3,}\s*$', multiLine: true), '');

  return markdown.split("\n").where((s)=>s.trim().isNotEmpty).toList().join("\n");
}

