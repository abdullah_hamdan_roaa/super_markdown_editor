import 'package:flutter/cupertino.dart';
import 'package:flutter_quill/flutter_quill.dart';
import 'package:super_markdown_editor/src/embed_quill_builders/table/table_block_embed.dart';
import 'package:super_markdown_editor/src/embed_quill_builders/table/table_embed_quill_builder.dart';
import 'package:super_markdown_editor/src/markdown_quill_converter/delta_to_markdown.dart';
import 'package:super_markdown_editor/src/markdown_quill_converter/markdown_to_delta.dart';
import 'package:markdown/markdown.dart' as md;

class QuillUtils {



  static final DeltaToMarkdown _deltaToMarkdown = DeltaToMarkdown();




   static String convertDocToMarkdown(Document doc) => _deltaToMarkdown.convert(doc.toDelta());

   static Document convertMarkdownToDocument(String markdown,double fontSize,String dir) {
    var  mdDocument = md.Document(encodeHtml: false );
      var mdToDelta = MarkdownToDelta(markdownDocument: mdDocument, fontSize: fontSize,
         dir: dir );
    return Document.fromDelta(mdToDelta.convert(markdown));
  }
}





