import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_quill/flutter_quill.dart' as quill;
import 'package:flutter_quill/translations.dart';
import 'package:super_markdown_editor/quill_utils.dart';
import 'package:super_markdown_editor/src/my_quill_embed.dart';

class SuperMarkdownEditor extends StatefulWidget {
  final TextEditingController textController;
  final ValueChanged<String> onChanged;
  final ValueChanged<String> onLaunchUrl;
  final TextStyle textStyle;
  final double minHeight;
  final BoxDecoration editorDecoration;
  final Future<String?> Function() onPickImage;
  final Future<Map<String, String>?> Function() onPickFile;
  final void Function(String) onDownloadClick;
  final FocusNode focusNode;
  final Color? primaryColor;
  final Widget Function(bool, VoidCallback) linkActionBuilder;
  final Function(BuildContext, Widget, VoidCallback) showTableBuilder;
  final bool viewMode;
  final String? hintText;
  final TextStyle? hintStyle;

  const SuperMarkdownEditor(
      {Key? key,
      required this.textController,
      required this.textStyle,
      required this.onChanged,
      required this.onPickImage,
      required this.onPickFile,
      required this.onDownloadClick,
      required this.focusNode,
      required this.onLaunchUrl,
      required this.showTableBuilder,
      this.primaryColor,
      required this.minHeight,
      required this.editorDecoration,
      required this.linkActionBuilder,
      required this.viewMode,
      this.hintText,
      this.hintStyle})
      : super(key: key);

  @override
  State<SuperMarkdownEditor> createState() => _SuperMarkdownEditorState();
}

class _SuperMarkdownEditorState extends State<SuperMarkdownEditor> {
  late quill.QuillController _quillController;
  late TextStyle _baseTextStyle;

  bool showEmoji = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    _quillController = _buildQuillController();
    _quillController.document.changes.listen((event) {
      String markdown = QuillUtils.convertDocToMarkdown(_quillController.document);
      widget.textController.text = updateMarkdown(markdown);
      widget.onChanged(updateMarkdown(markdown));
    });
    _baseTextStyle =
        TextStyle(fontFamily: widget.textStyle.fontFamily, color: widget.textStyle.color);
    _quillController
        .formatSelection(quill.Attribute.fromKeyValue('size', widget.textStyle.fontSize));
    super.didChangeDependencies();
  }

  String updateMarkdown(String normal) {
    List<String> newLines = [];
    List<String> lines = normal.split("\n");
    for (int i = 0; i < lines.length; i++) {
      String line = lines[i];
      if (line.contains("|") && !line.startsWith("|")) {
        newLines.add(line.replaceFirst("|", "\n|"));
      } else if (line.contains("|") && !line.endsWith("|")) {
        List<String> theNewLine = line.split("|");
        String extra = theNewLine.last;
        theNewLine.removeLast();
        newLines.add("${theNewLine.join("|")}|\n\n$extra");
      } else if (line.startsWith("|") &&
          line.endsWith("|") &&
          i > 0 &&
          i != lines.length - 2 &&
          line.split("|").length != lines[i - 1].split("|").length &&
          line.split("|").length != lines[i + 1].split("|").length) {
        List<String> theNewLine = line.split("|").take(lines[i - 1].split("|").length - 1).toList();
        String extra = line.split("|")[lines[i - 1].split("|").length - 1];
        List<String> newTable = line.split("|");
        newTable.removeRange(0, lines[i - 1].split("|").length);
        newLines.add("${theNewLine.join("|")}|\n\n$extra\n|${newTable.join("|")}");
      } else {
        newLines.add(line);
      }
    }
    return newLines.join("\n");
  }

  quill.QuillController _buildQuillController() {
    quill.QuillController controller;
    quill.Document? doc;
    if (widget.textController.text.isEmpty) {
      controller = quill.QuillController(
          document: quill.Document(),
          keepStyleOnNewLine: true,
          selection: const TextSelection.collapsed(offset: 0));
    } else {
      doc = QuillUtils.convertMarkdownToDocument(
          widget.textController.text,
          widget.textStyle.fontSize ?? 10,
          Directionality.of(context) == TextDirection.rtl ? "rtl" : "ltr");

      controller = quill.QuillController(
          keepStyleOnNewLine: true,
          document: doc,
          selection: TextSelection.collapsed(offset: doc.length - 1));
    }
    if (Directionality.of(context) == TextDirection.rtl) {
      controller.formatSelection(quill.Attribute.fromKeyValue('direction', "rtl"));
    }
    return controller;
  }

  Future<quill.LinkMenuAction> _showMaterialMenu(BuildContext context, String link) async {
    final result = await showModalBottomSheet<quill.LinkMenuAction>(
      context: context,
      builder: (ctx) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            _MaterialAction(
              title: 'Open'.i18n,
              icon: Icons.language_sharp,
              style: widget.textStyle.copyWith(fontSize: (widget.textStyle.fontSize ?? 12) + 4.0),
              onPressed: () => Navigator.of(context).pop(quill.LinkMenuAction.launch),
            ),
            _MaterialAction(
              title: 'Copy'.i18n,
              icon: Icons.copy_sharp,
              style: widget.textStyle.copyWith(fontSize: (widget.textStyle.fontSize ?? 12) + 4.0),
              onPressed: () => Navigator.of(context).pop(quill.LinkMenuAction.copy),
            ),
            _MaterialAction(
              title: 'Remove'.i18n,
              icon: Icons.link_off_sharp,
              style: widget.textStyle.copyWith(fontSize: (widget.textStyle.fontSize ?? 12) + 4.0),
              onPressed: () => Navigator.of(context).pop(quill.LinkMenuAction.remove),
            ),
          ],
        );
      },
    );

    return result ?? quill.LinkMenuAction.none;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          decoration: widget.editorDecoration,
          child: quill.QuillEditor(
              placeholder: widget.viewMode ? null : widget.hintText,
              minHeight: widget.minHeight,
              controller: _quillController,
              showCursor: widget.viewMode ? false : true,
              embedBuilders: MyQuillEmbeds.builders(
                  primaryColor: widget.primaryColor ?? Theme.of(context).primaryColor,
                  showTableBuilder: widget.showTableBuilder,
                  textStyle: _baseTextStyle,
                  onDownloadClick: widget.onDownloadClick),
              customStyles: quill.DefaultStyles(
                placeHolder: widget.hintStyle == null
                    ? null
                    : quill.DefaultTextBlockStyle(
                        widget.hintStyle!,
                        const quill.VerticalSpacing(1, 1),
                        const quill.VerticalSpacing(1, 1),
                        const BoxDecoration()),
                color: _baseTextStyle.color,
                bold: _baseTextStyle.copyWith(fontWeight: FontWeight.bold),
                italic: _baseTextStyle.copyWith(fontStyle: FontStyle.italic),
                link: _baseTextStyle.copyWith(fontWeight: FontWeight.bold, color: Colors.blue),
                underline: _baseTextStyle.copyWith(decoration: TextDecoration.underline),
                strikeThrough: _baseTextStyle.copyWith(decoration: TextDecoration.lineThrough),
                superscript: _baseTextStyle,
                subscript: _baseTextStyle,
                small: _baseTextStyle,
                inlineCode: quill.InlineCodeStyle(
                    style: _baseTextStyle.copyWith(
                        fontFamily: "Rob"
                            "oto Mono",
                        backgroundColor: Colors.grey.shade300),
                    backgroundColor: Colors.grey,
                    radius: Radius.circular(10)),
                h1: quill.DefaultListBlockStyle(
                    _baseTextStyle.copyWith(fontSize: ((_baseTextStyle.fontSize ?? 12) + 12)),
                    const quill.VerticalSpacing(12, 12),
                    const quill.VerticalSpacing(12, 12),
                    const BoxDecoration(),
                    null),
                h2: quill.DefaultListBlockStyle(
                    _baseTextStyle.copyWith(fontSize: ((_baseTextStyle.fontSize ?? 12) + 6)),
                    const quill.VerticalSpacing(6, 6),
                    const quill.VerticalSpacing(6, 6),
                    const BoxDecoration(),
                    null),
                h3: quill.DefaultListBlockStyle(
                    _baseTextStyle.copyWith(fontSize: ((_baseTextStyle.fontSize ?? 12) + 2)),
                    const quill.VerticalSpacing(2, 2),
                    const quill.VerticalSpacing(2, 2),
                    const BoxDecoration(),
                    null),
                lists: quill.DefaultListBlockStyle(
                    _baseTextStyle,
                    const quill.VerticalSpacing(4, 4),
                    const quill.VerticalSpacing(4, 4),
                    const BoxDecoration(),
                    null),
                leading: quill.DefaultListBlockStyle(
                    _baseTextStyle,
                    const quill.VerticalSpacing(4, 4),
                    const quill.VerticalSpacing(4, 4),
                    const BoxDecoration(),
                    null),
                paragraph: quill.DefaultListBlockStyle(
                    _baseTextStyle,
                    const quill.VerticalSpacing(1, 1),
                    const quill.VerticalSpacing(1, 1),
                    const BoxDecoration(),
                    null),
                quote: quill.DefaultListBlockStyle(
                    _baseTextStyle,
                    const quill.VerticalSpacing(1, 1),
                    const quill.VerticalSpacing(1, 1),
                    BoxDecoration(
                        border: BorderDirectional(
                            start: BorderSide(width: 4, color: Colors.grey.shade400))),
                    null),
              ),
              focusNode: widget.focusNode,
              enableUnfocusOnTapOutside: true,
              onLaunchUrl: widget.onLaunchUrl,
              scrollController: ScrollController(),
              scrollable: true,
              padding: const EdgeInsets.all(8),
              autoFocus: false,
              linkActionPickerDelegate: (context, link, node) {
                return _showMaterialMenu(context, link);
              },
              readOnly: widget.viewMode,
              expands: false),
        ),
        if (widget.viewMode == false)
          quill.QuillToolbar.basic(
            controller: _quillController,
            embedButtons: MyQuillEmbeds.buttons(
                primaryColor: widget.primaryColor ?? Theme.of(context).primaryColor,
                textStyle: _baseTextStyle,
                showTableBuilder: widget.showTableBuilder,
                onPickFile: widget.onPickFile,
                onPickImage: widget.onPickImage,
                onEmojiPressed: () {
                  setState(() {
                    showEmoji = !showEmoji;
                  });
                }),
            linkDialogAction: quill.LinkDialogAction(builder: widget.linkActionBuilder),
            iconTheme: quill.QuillIconTheme(
              iconSelectedFillColor: widget.primaryColor,
            ),
            afterButtonPressed: () {
              if (_quillController.getSelectionStyle().attributes.keys.contains("header")) {
                _quillController.formatSelection(quill.Attribute.fromKeyValue('size', null));
              } else {
                _quillController.formatSelection(
                    quill.Attribute.fromKeyValue('size', widget.textStyle.fontSize));
              }
            },
            showAlignmentButtons: false,
            showBackgroundColorButton: false,
            showSubscript: false,
            showSuperscript: false,
            showInlineCode: true,
            showUndo: false,
            showSmallButton: false,
            showRedo: false,
            showSearchButton: false,
            showUnderLineButton: false,
            showClearFormat: true,
            showHeaderStyle: true,
            showColorButton: false,
            showFontFamily: false,
            showFontSize: false,
            showCodeBlock: false,
            showIndent: false,
            showDividers: false,
            showListCheck: false,
            multiRowsDisplay: false,
          ),
        const SizedBox(height: 20),
        if (showEmoji)
          SizedBox(
            height: 300,
            child: EmojiPicker(
              config: const Config(
                  columns: 8, emojiSizeMax: 30, horizontalSpacing: 10, verticalSpacing: 10),
              onEmojiSelected: (_, emoji) {
                _insertEmoji(emoji.emoji);
              },
            ),
          )
      ],
    );
  }

  void _insertEmoji(String emoji) {
    _insertText(emoji);
  }

  void _insertText(String text) {
    final index = _quillController.selection.baseOffset;
    final length = _quillController.selection.extentOffset - index;
    _quillController.replaceText(index, length, text, null);
    _quillController.updateSelection(
        TextSelection.collapsed(offset: index + text.length), quill.ChangeSource.LOCAL);
  }
}

class SimpleMarkdownEditor extends StatefulWidget {
  final TextEditingController textController;
  final quill.QuillController controller;
  final ValueChanged<String> onChanged;
  final ValueChanged<String> onLaunchUrl;
  final TextStyle textStyle;
  final FocusNode focusNode;
  final Color? primaryColor;

  const SimpleMarkdownEditor(
      {Key? key,
      required this.textController,
      required this.textStyle,
      required this.onChanged,
      required this.focusNode,
      required this.onLaunchUrl,
      this.primaryColor,
      required this.controller})
      : super(key: key);

  @override
  State<SimpleMarkdownEditor> createState() => _SimpleMarkdownEditorState();
}

class _SimpleMarkdownEditorState extends State<SimpleMarkdownEditor> {
  bool showEmoji = false;
  late TextStyle _baseTextStyle;

  @override
  void initState() {
    widget.controller.document.changes.listen((event) {
      String markdown = QuillUtils.convertDocToMarkdown(widget.controller.document);
      widget.textController.text = markdown;
      widget.onChanged(markdown);
    });
    _baseTextStyle =
        TextStyle(fontFamily: widget.textStyle.fontFamily, color: widget.textStyle.color);
    widget.controller
        .formatSelection(quill.Attribute.fromKeyValue('size', widget.textStyle.fontSize));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        quill.QuillEditor(
            controller: widget.controller,
            focusNode: widget.focusNode,
            onLaunchUrl: widget.onLaunchUrl,
            customStyles: quill.DefaultStyles(
              color: _baseTextStyle.color,
              bold: _baseTextStyle.copyWith(fontWeight: FontWeight.bold),
              italic: _baseTextStyle.copyWith(fontStyle: FontStyle.italic),
              link: _baseTextStyle.copyWith(fontWeight: FontWeight.bold, color: Colors.blue),
              underline: _baseTextStyle.copyWith(decoration: TextDecoration.underline),
              strikeThrough: _baseTextStyle.copyWith(decoration: TextDecoration.lineThrough),
              superscript: _baseTextStyle,
              subscript: _baseTextStyle,
              small: _baseTextStyle,
              h1: quill.DefaultListBlockStyle(
                  _baseTextStyle.copyWith(fontSize: ((_baseTextStyle.fontSize ?? 12) + 12)),
                  const quill.VerticalSpacing(12, 12),
                  const quill.VerticalSpacing(12, 12),
                  const BoxDecoration(),
                  null),
              h2: quill.DefaultListBlockStyle(
                  _baseTextStyle.copyWith(fontSize: ((_baseTextStyle.fontSize ?? 12) + 6)),
                  const quill.VerticalSpacing(6, 6),
                  const quill.VerticalSpacing(6, 6),
                  const BoxDecoration(),
                  null),
              h3: quill.DefaultListBlockStyle(
                  _baseTextStyle.copyWith(fontSize: ((_baseTextStyle.fontSize ?? 12) + 2)),
                  const quill.VerticalSpacing(2, 2),
                  const quill.VerticalSpacing(2, 2),
                  const BoxDecoration(),
                  null),
              lists: quill.DefaultListBlockStyle(_baseTextStyle, const quill.VerticalSpacing(4, 4),
                  const quill.VerticalSpacing(4, 4), const BoxDecoration(), null),
              leading: quill.DefaultListBlockStyle(
                  _baseTextStyle,
                  const quill.VerticalSpacing(4, 4),
                  const quill.VerticalSpacing(4, 4),
                  const BoxDecoration(),
                  null),
              paragraph: quill.DefaultListBlockStyle(
                  _baseTextStyle,
                  const quill.VerticalSpacing(1, 1),
                  const quill.VerticalSpacing(1, 1),
                  const BoxDecoration(),
                  null),
              quote: quill.DefaultListBlockStyle(
                  _baseTextStyle,
                  const quill.VerticalSpacing(1, 1),
                  const quill.VerticalSpacing(1, 1),
                  BoxDecoration(
                      border: BorderDirectional(
                          start: BorderSide(width: 4, color: Colors.grey.shade400))),
                  null),
            ),
            scrollController: ScrollController(),
            scrollable: true,
            padding: const EdgeInsets.all(8),
            autoFocus: false,
            readOnly: false,
            expands: false),
      ],
    );
  }
}

class _MaterialAction extends StatelessWidget {
  const _MaterialAction({
    required this.title,
    required this.icon,
    required this.onPressed,
    Key? key,
    required this.style,
  }) : super(key: key);

  final String title;
  final IconData icon;
  final VoidCallback onPressed;
  final TextStyle style;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              icon,
              size: 16,
              color: style.color,
            ),
            const SizedBox(
              width: 8,
            ),
            Text(title, style: style),
          ],
        ),
      ),
    );
  }
}
