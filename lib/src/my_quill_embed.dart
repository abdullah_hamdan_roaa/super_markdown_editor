import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter_quill/flutter_quill.dart';
import 'package:super_markdown_editor/src/embed_quill_builders/emoji/emoji_embed_quill_builder.dart';
import 'package:super_markdown_editor/src/embed_quill_builders/emoji/emoji_quill_button.dart';
import 'package:super_markdown_editor/src/embed_quill_builders/file/file_embed_quill_builder.dart';
import 'package:super_markdown_editor/src/embed_quill_builders/file/file_quill_button.dart';
import 'package:super_markdown_editor/src/embed_quill_builders/image/image_embed_quill_builder.dart';
import 'package:super_markdown_editor/src/embed_quill_builders/image/image_quill_button.dart';
import 'package:super_markdown_editor/src/embed_quill_builders/table/table_embed_quill_builder.dart';
import 'package:super_markdown_editor/src/embed_quill_builders/table/table_quill_button.dart';

class MyQuillEmbeds {
  static List<EmbedBuilder> builders({
  required Color primaryColor,
  required  Function(String) onDownloadClick,
  required  TextStyle textStyle,
    required Function(BuildContext, Widget, VoidCallback)
    showTableBuilder}) => [
        ImageEmbedQuillBuilder(),
        FileEmbedQuillBuilder(onDownloadClick: onDownloadClick, textStyle: textStyle),
        TableEmbedQuillBuilder(passedTable: null,showTableBuilder: showTableBuilder, primaryColor: primaryColor, textStyle: textStyle),
        EmojiEmbedQuillBuilder()
      ];

  static List<EmbedButtonBuilder> buttons(
          {required Future<String?> Function() onPickImage,
          required Future<Map<String,String>?> Function() onPickFile,
  required Color primaryColor,

  required Function(BuildContext, Widget, VoidCallback) showTableBuilder,
            required TextStyle textStyle,
          required VoidCallback onEmojiPressed}) =>
      [
        (controller, toolbarIconSize, iconTheme, dialogTheme) => TableQuillButton(
            icon: Icons.table_chart,
            iconSize: toolbarIconSize,
            controller: controller,
            iconTheme: iconTheme,
            dialogTheme: dialogTheme,
           primaryColor: primaryColor,
           textStyle: textStyle,
           showTableBuilder: showTableBuilder,
        ),
        (controller, toolbarIconSize, iconTheme, dialogTheme) => ImageQuillButton(
              icon: Icons.image,
              iconSize: toolbarIconSize,
              controller: controller,
              iconTheme: iconTheme,
              dialogTheme: dialogTheme,
              onPickImage: onPickImage,
            ),
        (controller, toolbarIconSize, iconTheme, dialogTheme) => FileQuillButton(
              icon: Icons.attach_file_rounded,
              iconSize: toolbarIconSize,
              controller: controller,
              iconTheme: iconTheme,
              dialogTheme: dialogTheme,
              onPickFile: onPickFile,
            ),
        (controller, toolbarIconSize, iconTheme, dialogTheme) => EmojiQuillButton(
              icon: Icons.emoji_emotions_sharp,
              iconSize: toolbarIconSize,
              controller: controller,
              iconTheme: iconTheme,
              dialogTheme: dialogTheme,
              onEmojiPressed: onEmojiPressed,
            )
      ];
}
