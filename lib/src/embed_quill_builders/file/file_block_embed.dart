import 'dart:convert';
import 'package:flutter_quill/flutter_quill.dart';

class FileBlockEmbed extends CustomBlockEmbed {
  static const String fileType = 'file';

  final String fileUrl;
  final String fileName;

  FileBlockEmbed({required this.fileUrl, required this.fileName})
      : super(fileType, '📎 [$fileName]($fileUrl)');

  static FileBlockEmbed fromDocument(Document document) {
    final jsonData = document.toDelta().toJson();
    final decodedData = jsonDecode(jsonData.toString());
    if (decodedData.containsKey('fileUrl') && decodedData.containsKey('fileName')) {
      final fileUrl = decodedData['fileUrl'] as String;
      final fileName = decodedData['fileName'] as String;
      return FileBlockEmbed(fileUrl: fileUrl,fileName:  fileName);
    }
    return FileBlockEmbed(fileUrl: '',fileName:  '');
  }


}
