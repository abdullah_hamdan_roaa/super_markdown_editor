import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_quill/flutter_quill.dart' hide Text;
import 'package:super_markdown_editor/src/embed_quill_builders/file/file_block_embed.dart';

class EmojiEmbedQuillBuilder extends EmbedBuilder {
  EmojiEmbedQuillBuilder();

  @override
  String get key => FileBlockEmbed.fileType;

  @override
  bool get expanded => false;

  @override
  Widget build(
    BuildContext context,
    QuillController controller,
    Embed node,
    bool readOnly,
    bool inline,
    TextStyle textStyle,
  ) {
    return Text(node.value.data);
  }
}
