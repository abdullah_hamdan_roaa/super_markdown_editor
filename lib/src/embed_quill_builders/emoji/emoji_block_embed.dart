import 'package:flutter_quill/flutter_quill.dart';

class EmojiBlockEmbed extends CustomBlockEmbed {
  static const String emojiType = 'emoji';

  final String emoji;

  EmojiBlockEmbed({required this.emoji})
      : super(emojiType, emoji);

  static EmojiBlockEmbed fromDocument(Document document) {
    final data = document.toDelta().toJson();
    return EmojiBlockEmbed(emoji: data[0]['insert'] as String);
  }

  @override
  Map<String, dynamic> toJson() {
    return {'insert': emoji};
  }
}