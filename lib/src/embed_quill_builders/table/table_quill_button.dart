import 'package:flutter/material.dart';
import 'package:flutter_quill/flutter_quill.dart' as quill;
import 'package:flutter_quill/flutter_quill.dart';
import 'package:super_markdown_editor/src/embed_quill_builders/table/table_block_embed.dart';
import 'package:super_markdown_editor/src/embed_quill_builders/table/table_builder_widget.dart';

class TableQuillButton extends StatelessWidget {

  const TableQuillButton({
    required this.icon,
    required this.controller,
    this.iconSize = quill.kDefaultIconSize,
    this.fillColor,
    this.iconTheme,
    this.dialogTheme,
    this.tooltip,
    this.linkRegExp,
    Key? key, required this.showTableBuilder, required this.primaryColor, required this.textStyle,
  }) : super(key: key);

  final IconData icon;
  final Color primaryColor;

  final double iconSize;
  final TextStyle textStyle;
  final Color? fillColor;
  final quill.QuillController controller;
  final quill.QuillIconTheme? iconTheme;
  final quill.QuillDialogTheme? dialogTheme;
  final String? tooltip;
  final RegExp? linkRegExp;
  final Function(BuildContext, Widget, VoidCallback) showTableBuilder;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    final iconColor = iconTheme?.iconUnselectedColor ?? theme.iconTheme.color;
    final iconFillColor = iconTheme?.iconUnselectedFillColor ?? (fillColor ?? theme.canvasColor);

    return quill.QuillIconButton(
      icon: Icon(icon, size: iconSize, color: iconColor),
      tooltip: tooltip,
      highlightElevation: 0,
      hoverElevation: 0,
      size: iconSize * 1.77,
      fillColor: iconFillColor,
      borderRadius: iconTheme?.borderRadius ?? 2,
      onPressed: () {
        _onPressedHandler(context);
      },
    );
  }

  void _onPressedHandler(BuildContext context) {
    List<List<String>> table = [];
    showTableBuilder(
      context,
      TableBuilderWidget(
        textStyle: textStyle,
        onTableDataChanged: (tableData) {
          table = tableData;
        }, primaryColor: primaryColor,
      ),
      () {
        if(table.isNotEmpty) {
          _insertTable(table);
        }
        Navigator.pop(context);
      },
    );
  }

  void _insertTable(List<List<String>> tableData) {
    final tableBlockEmbed = TableBlockEmbed(tableData);
    controller.replaceText(controller.selection.baseOffset, 0, tableBlockEmbed, null);
    final imagePosition = controller.document.length - 1;
    controller.updateSelection(TextSelection.collapsed(offset: imagePosition), ChangeSource.LOCAL);
  }
}
