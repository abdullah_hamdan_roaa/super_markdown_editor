import 'dart:convert';
import 'package:flutter_quill/flutter_quill.dart';

class TableBlockEmbed extends CustomBlockEmbed {
  static const String tableType = 'table';

  TableBlockEmbed(List<List<String>> tableData)
      : super(tableType, _encodeTableData(tableData));

  static String _encodeTableData(List<List<String>> tableData) {
    final StringBuffer markdownTable = StringBuffer();

    if (tableData.isNotEmpty) {
      // Add header row
      final headerRow = tableData[0].join(' | ');
      markdownTable.writeln('| $headerRow |');

      // Add separator row
      final separatorRow = tableData[0].map((cell) => '---').join(' | ');
      markdownTable.writeln('| $separatorRow |');

      // Add data rows
      for (int i = 1; i < tableData.length; i++) {
        final rowData = tableData[i].join(' | ');
        markdownTable.writeln('| $rowData |');
      }
    }

    return markdownTable.toString();
  }

  static List<List<String>> _decodeTableData(String markdownTable) {
    List<List<String>> tableData = [];
    List<String> lines = markdownTable.trim().split('\n');
    lines.removeAt(1);

    for (String line in lines) {
      List<String> s = line.split("|");
      s.removeAt(0);
      s.removeLast();
      List<String> cells = s.map((cell) => cell.trim()).toList();

      tableData.add(cells);
    }
    return tableData;
  }


  static TableBlockEmbed fromString(String data) {
    return TableBlockEmbed(_decodeTableData(data));
  }

  static TableBlockEmbed fromDocument(Document document) {
    final jsonData = document.toDelta().toJson();
    final decodedData = jsonDecode(jsonData.toString());
    if (decodedData.containsKey('tableData')) {
      final tableData = decodedData['tableData'] as List;
      return TableBlockEmbed(tableData.map<List<String>>((row) {
        return (row as List).map<String>((cell) => cell.toString()).toList();
      }).toList());
    }
    return TableBlockEmbed([]);
  }

  List<List<String>> getTableData() {
    final list = _decodeTableData(data);
    return list;
  }

  TableBlockEmbed updateTableData(List<List<String>> newTableData) {
    final jsonData = jsonDecode(data);
    jsonData['tableData'] = newTableData;
    return TableBlockEmbed(newTableData);
  }

  Document toDocument() {
    final jsonData = jsonDecode(data);
    final deltaData = Delta.fromJson(jsonData);
    return Document.fromDelta(deltaData);
  }
}
